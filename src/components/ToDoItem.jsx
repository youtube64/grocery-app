import React from "react";

function ToDoItem(props) {
  return (
    <div className="items">
      <li>
        <img
          src="tick.png"
          alt="tick"
          width="30"
          onClick={() => {
            props.onChecked(props.id);
          }}
        />{" "}
        {props.text}
        <img
          className="drag"
          align="right"
          src="drag.png"
          width="20"
          alt="drag"
        />
      </li>
    </div>
  );
}

export default ToDoItem;
