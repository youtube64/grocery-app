import React, { useEffect, useState } from "react";
import ToDoItem from "./ToDoItem";

function App() {
  console.log(typeof localStorage.getItem("todos"));

  const [task, setTask] = useState("");
  const [items, setItems] = useState(
    JSON.parse(localStorage.getItem("todos")) || []
  );

  useEffect(() => {
    localStorage.setItem("todos", JSON.stringify(items));
  }, [items]);

  function handleChange(event) {
    const newValue = event.target.value;
    setTask(newValue);
  }

  function removeDuplicates(arr) {
    let uniqueArr = arr.filter((item, index) => arr.indexOf(item) === index);
    uniqueArr = uniqueArr.map((item) => item.toLowerCase());

    return uniqueArr;
  }

  function addTask() {
    let tasks = Array.isArray(task.split(",")) ? task.split(",") : [task];
    setItems((prevValues) => {
      let d = [...tasks, ...prevValues];
      d = removeDuplicates(d);
      return d;
    });
    setTask("");
  }

  function deleteItem(id) {
    setItems((prevValues) => {
      return prevValues.filter((item, index) => {
        return index !== id;
      });
    });
  }

  return (
    <div className="container">
      <div className="heading">
        <h1>Grocery List</h1>
      </div>
      <div className="form">
        <input
          name="taskInput"
          type="text"
          onChange={handleChange}
          value={task}
        />
        <button onClick={addTask}>
          <span>ADD</span>
        </button>
      </div>
      <div className="data">
        <ul>
          {items.map((item, index) => (
            <ToDoItem
              key={index}
              id={index}
              text={item}
              onChecked={deleteItem}
            />
          ))}
        </ul>
      </div>
    </div>
  );
}

export default App;
